/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   envcontroller.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/27 20:29:22 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/18 21:09:23 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>

int			env_count(t_env *env)
{
	int		i;

	i = 0;
	while (env->envp[i])
		i++;
	return (i);
}

static void	env_ctrl_nenv(t_sh *sh, char **args, int opt, int i)
{
	t_env	*tmp_env;
	t_env	*old_env;

	tmp_env = NULL;
	if (!(tmp_env = (t_env *)ft_memalloc(sizeof(t_env))))
		return ;
	tmp_env->envp = (opt) ? (char **)ft_memalloc(sizeof(char *) * 1) :
		sh->env->envp;
	env_copy(tmp_env);
	while (ft_strchr(args[i], '='))
	{
		env_set(args[i], tmp_env);
		i++;
	}
	if (args[i])
	{
		old_env = sh->env;
		sh->env = tmp_env;
		sh_ctrl_exe(sh, args + i);
		sh->env = old_env;
	}
	else
		sh->view->display(tmp_env);
	free(tmp_env);
}

int			env_ctrl(t_sh *sh, char **args)
{
	int					i;
	unsigned int		opt;
	int					errflags;

	errflags = 0;
	opt = 0;
	if (!args[1] || (!ft_strcmp("--", args[1]) && !args[2]))
		sh->view->display(sh->env);
	else
	{
		if ((errflags = ft_searchflags(ft_arrlen((void **)args), args,
			ft_strdup("-i"), &opt)) < 0)
		{
			ft_putstr_fd("env: illegal option: -", 2);
			ft_putchar_fd(-errflags, 2);
			ft_putchar_fd('\n', 2);
			return (-1);
		}
		i = 1;
		while (args[i] && ft_isoption(args[i]))
			i++;
		env_ctrl_nenv(sh, args, opt, i);
	}
	return (0);
}
