/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shmanager.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 03:16:41 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/16 04:45:12 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>

static void		sh_hydrate_path(t_sh *sh)
{
	int		j;
	int		i;
	char	*tmp;

	i = -1;
	if (!env_isset("PATH=", sh->env))
		sh->path = NULL;
	else
	{
		while (sh->env->envp[++i])
		{
			if (!ft_strncmp(sh->env->envp[i], "PATH=", 5))
			{
				tmp = ft_strdup(sh->env->envp[i] + 5);
				sh->path = ft_strsplit(tmp, ':');
				j = -1;
				while (sh->path[++j])
				{
					tmp = ft_strjoin(sh->path[j], "/");
					free(sh->path[j]);
					sh->path[j] = tmp;
				}
			}
		}
	}
}

void			sh_hydrate(t_sh *sh)
{
	int		i;

	i = -1;
	sh_hydrate_path(sh);
	if (env_isset("OLDPWD=", sh->env))
		sh->oldpwd = NULL;
	if (env_isset("HOME=", sh->env))
		sh->home = NULL;
	while (sh->env->envp[++i])
	{
		if (!ft_strncmp(sh->env->envp[i], "PWD=", 4))
			sh->pwd = ft_strdup(sh->env->envp[i] + 4);
		if (!ft_strncmp(sh->env->envp[i], "OLDPWD=", 7))
			sh->oldpwd = ft_strdup(sh->env->envp[i] + 7);
		if (!ft_strncmp(sh->env->envp[i], "HOME=", 5))
			sh->home = ft_strdup(sh->env->envp[i] + 5);
		if (!ft_strncmp(sh->env->envp[i], "SHLVL=", 6))
			sh->lvl = ft_atoi(sh->env->envp[i] + 6);
	}
}

void			sh_run(t_sh *sh)
{
	char	*cmd;
	int		running;

	running = 1;
	while (running)
	{
		while ((cmd = sh->view->getcmd(sh->view)) &&
				(cmd[0] == '\0') && cmd[0] == '\n')
			break ;
		if ((cmd) && (*cmd))
			sh_parse_command(cmd, sh);
		else
			ft_putchar('\n');
	}
}
