/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signalhandler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/29 20:07:53 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/16 03:16:11 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>
#include <sys/types.h>

static void		handle_c(void)
{
	return ;
}

void			signal_handler(int signum)
{
	if (signum == SIGQUIT)
		handle_c();
	else if (signum == SIGINT)
		handle_c();
}

void			wif_msg(int status)
{
	if (WIFSIGNALED(status))
	{
		if (WTERMSIG(status) == SIGSEGV)
			ft_putendl_fd("sh: [ERROR] Segmentation fault. N00b.", 2);
	}
}

void			signal_catch(void)
{
	signal(SIGQUIT, &signal_handler);
	signal(SIGINT, &signal_handler);
	signal(SIGSEGV, SIG_DFL);
}
