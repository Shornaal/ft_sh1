/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   envmanager.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 03:20:27 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/18 20:24:21 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh1.h"

void	env_copy(t_env *env)
{
	char	**tmp_env;
	int		i;

	i = 0;
	while (env->envp[i])
		i++;
	if (!(tmp_env = (char **)malloc(sizeof(char *) * i + 1)))
		return ;
	tmp_env[i] = NULL;
	while (--i >= 0)
		tmp_env[i] = ft_strdup(env->envp[i]);
	env->envp = tmp_env;
}

void	env_new(t_env *env, int idx, char *args)
{
	char	**tmp_env;
	int		i;

	tmp_env = NULL;
	if (!(tmp_env = (char **)ft_memalloc(sizeof(char *) * (idx + 2))))
		return ;
	i = 0;
	while (env->envp[i])
	{
		tmp_env[i] = ft_strdup(env->envp[i]);
		i++;
	}
	tmp_env[i] = ft_strdup(args);
	tmp_env[i + 1] = NULL;
	free(env->envp);
	env->envp = tmp_env;
}

void	env_unset(char *name, t_env *env)
{
	char	**tmp_env;
	int		j;
	int		i;

	i = -1;
	j = env->count(env);
	name = (name[0] != '=') ? ft_strjoin(name, "=") : name;
	if ((tmp_env = (char **)ft_memalloc(sizeof(char *) * j + 1)) == NULL)
		return ;
	j = 0;
	while ((env->envp[j]))
	{
		if (ft_strncmp(env->envp[j], name, ft_strlen(name)))
			tmp_env[++i] = ft_strdup(env->envp[j]);
		j++;
	}
	tmp_env[i + 1] = NULL;
	free(name);
	free(env->envp);
	env->envp = tmp_env;
}

void	env_set(char *args, t_env *env)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while ((args[i] != '=') && (args[i] != '\0'))
		i++;
	if (args[i] == '\0')
	{
		ft_putstr_fd("Setenv: Error '", 2);
		ft_putstr_fd(args, 2);
		ft_putendl_fd("' use has NAME=VALUE.", 2);
		return ;
	}
	while ((env->envp[j]) && (ft_strncmp(env->envp[j], args, i + 1)))
		j++;
	if (env->envp[j])
	{
		free(env->envp[j]);
		env->envp[j] = ft_strdup(args);
	}
	else
		env_new(env, j, args);
}

void	env_cd(t_sh *sh)
{
	char	*tmp;
	char	*pwd;
	char	cwd[4096];

	tmp = ft_strjoin("OLDPWD=", sh->pwd);
	env_set(tmp, sh->env);
	if (tmp)
		free(tmp);
	ft_bzero(cwd, 4096);
	tmp = getcwd(cwd, BUFF_SIZE);
	pwd = ft_strjoin("PWD=", tmp);
	env_set(pwd, sh->env);
	sh->hydrate(sh);
	if (pwd)
		free(pwd);
}
