/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shcd.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 19:30:12 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/16 06:29:28 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>

static void			err_cd(const char *path, char *msg)
{
	ft_putstr_fd(msg, 2);
	ft_putendl_fd(path, 2);
}

static int			sh_cd_rigth(const char *path)
{
	if ((access(path, F_OK)) == -1)
	{
		err_cd(path, "cd: no such file or directory: ");
		return (0);
	}
	else if (!file_isdir(path))
		err_cd(path, "cd: Not a directory: ");
	else if ((access(path, R_OK)) == -1)
	{
		err_cd(path, "cd: permission denied: ");
		return (0);
	}
	return (1);
}

static void			set_tild_path(t_sh *sh, char **args, char *currpath)
{
	if (!(sh->home))
	{
		ft_putendl_fd("No $home variable set.", 2);
		return ;
	}
	currpath = ft_strjoin(sh->home, "/");
	if (!ft_strcmp(args[1], "--") && (args[2]))
		currpath = ft_strjoin(currpath, args[2]);
	ft_putendl(currpath);
	args[1] = currpath;
	free(currpath);
}

char				*get_currpath(t_sh *sh, char **args)
{
	char	*currpath;

	currpath = NULL;
	if ((args[1][0] == '~') || !(ft_strcmp(args[1], "--")))
		set_tild_path(sh, args, currpath);
	if ((args[1][0] == '-') && (args[1][1]) == '\0')
	{
		if ((sh->oldpwd) && (sh->home))
		{
			currpath = (ft_strcmp(sh->oldpwd, sh->home)) ? ft_strdup(sh->oldpwd
					+ ft_strlen(sh->home)) : ft_strdup(sh->oldpwd);
		}
		else
			currpath = ft_strdup("");
		args[1] = (sh->oldpwd) ? sh->oldpwd : currpath;
		free(currpath);
	}
	return (currpath);
}

int					sh_cd(t_sh *sh, char **args)
{
	if (args[1])
	{
		get_currpath(sh, args);
		if (!sh_cd_rigth(args[1]))
			return (-1);
		if (!chdir(args[1]))
			env_cd(sh);
	}
	else
	{
		if (chdir(sh->home))
			err_cd(sh->home, "cd: no such file or directory: ");
		else
			env_cd(sh);
	}
	return (0);
}
