/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_handlerinputs.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/17 12:15:00 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/18 21:05:10 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>

int				ft_isoption(const char *s)
{
	if (!ft_strcmp("--", s))
		return (1);
	if (s[0] == '-' && ((s[1] != '\0' || s[1] == ' ')))
		return (1);
	return (0);
}

static void		ft_setoption(int flag, unsigned int *options)
{
	if (flag == 105)
		*options = 1;
}

int				ft_searchflags(int argc, char **argv, char *search,
		unsigned int *options)
{
	size_t	i;
	size_t	j;
	size_t	k;

	i = 0;
	if (argc == 1 || !search)
		return (0);
	while (++i < (size_t)argc && ft_isoption(argv[i]))
	{
		j = -1;
		k = 1;
		while (search[++j] != '\0')
			if (argv[i][k] == search[j])
			{
				ft_setoption(argv[i][k], options);
				j = -1;
				k++;
			}
		if ((ft_strchr(search, argv[i][k]) == NULL))
			return (-(argv[i][k]));
	}
	return (0);
}
