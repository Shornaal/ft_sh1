/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/23 17:22:31 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/12 21:15:26 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>

int	main(int argc, char **argv, char **envp)
{
	t_sh	*sh;

	(void)argv;
	(void)argc;
	if (!(sh = sh_init(envp)) && !(argc))
		return (-1);
	signal_catch();
	sh->run(sh);
	return (0);
}
