/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shexecute.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/28 18:14:55 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/18 21:06:14 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>
#include <stdio.h>

static	void	sh_exec_err(char **args)
{
	ft_putstr(CRED);
	ft_putstr("(>T_T)> ~ ");
	ft_putstr(CCLR);
	ft_putstr_fd("sh: command not found: ", 2);
	ft_putendl_fd(args[0], 2);
}

static int		sh_execve_bin(char *path, char **args, char **envp, pid_t cpid)
{
	int curr_exe;

	if (access(path, F_OK) == -1)
	{
		ft_putstr_fd("sh: no such file or directory: ", 2);
		ft_putendl_fd(path, 2);
		exit(cpid);
		return (-1);
	}
	else if (access(path, X_OK) == -1)
	{
		ft_putstr_fd("sh: permission denied: ", 2);
		ft_putendl_fd(path, 2);
		exit(cpid);
		return (-1);
	}
	if ((curr_exe = execve(path, args, envp)))
	{
		sh_exec_err(args);
		exit(cpid);
		return (-1);
	}
	return (curr_exe);
}

int				sh_exec_bin(t_sh *sh, char *bin, char **args, char *path)
{
	int		curr_exe;
	int		status;
	pid_t	cpid;

	curr_exe = -1;
	if ((cpid = fork()) < 0)
		return (-1);
	else if (cpid == 0)
	{
		if (curr_exe < 0 && path && (bin[0] != '\0'))
		{
			path = ft_strjoin(path, "/");
			curr_exe = sh_execve_bin((ft_strcmp(path, bin)) ? bin :
					ft_strjoin(path, bin), args, sh->env->envp, cpid);
		}
	}
	else
		wait(&status);
	wif_msg(status);
	return (1);
}

static int		sh_execve(t_sh *sh, char *bin, char **args, pid_t cpid)
{
	int		curr_exe;
	int		i;

	i = -1;
	curr_exe = -1;
	while (curr_exe < 0 && sh->path[++i])
	{
		if ((curr_exe = execve(ft_strjoin(sh->path[i], bin), args,
			sh->env->envp)) && (sh->path[i + 1] == NULL) && (bin[0] != '\0'))
		{
			sh_exec_err(args);
			exit(cpid);
			return (-1);
		}
	}
	return (1);
}

int				sh_exec(t_sh *sh, char *bin, char **args)
{
	int		status;
	pid_t	cpid;

	if ((cpid = fork()) < 0)
		return (-1);
	else if (cpid == 0)
	{
		if (!sh_execve(sh, bin, args, cpid))
			return (-1);
	}
	else
		wait(&status);
	wif_msg(status);
	return (1);
}
