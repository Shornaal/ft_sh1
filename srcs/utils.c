/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/11 21:24:44 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/16 04:39:05 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>

char	*sh_getcwd(void)
{
	static char	buffer[BUFF_SIZE];

	ft_bzero(buffer, BUFF_SIZE);
	getcwd(buffer, BUFF_SIZE);
	return (buffer);
}

int		env_isset(char *name, t_env *env)
{
	int		i;

	i = -1;
	while (env->envp[++i])
	{
		if (!ft_strncmp(env->envp[i], name, ft_strlen(name)))
			return (1);
	}
	return (0);
}

int		file_isdir(const char *path)
{
	struct stat	sb;

	if (!path || path[0] == '\0')
		return (-1);
	if (stat(path, &sb) == 0)
	{
		if (sb.st_mode & S_IFDIR)
			return (1);
	}
	return (0);
}
