/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inputshandler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/23 17:22:17 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/16 03:26:57 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>

char	*sh_get_command(t_view *view)
{
	int		ret;
	char	*buffer;

	ret = 0;
	ft_putstr(CPINK);
	ft_putstr(view->ps1);
	ft_putstr(CCLR);
	if (!(buffer = (char *)ft_memalloc(BUFF_SIZE)))
		return (NULL);
	if ((ret = read(1, buffer, BUFF_SIZE)) > 0)
		buffer[ret] = '\0';
	return (buffer);
}

int		sh_parse_command(char *cmd, t_sh *sh)
{
	char	**cmds;
	char	**args;
	int		i;
	int		j;

	cmd = ft_strtrim(cmd);
	if (!cmd || (ft_iswhitespace(cmd[0])) || (cmd[0] == '\0'))
		return (0);
	cmds = ft_strsplit(cmd, ';');
	i = -1;
	while (cmds[++i])
	{
		cmds[i] = ft_strtrim(cmds[i]);
		ft_creplace(cmds[i], '\t', ' ');
		args = ft_strsplit(cmds[i], ' ');
		j = -1;
		while (args[++j])
			args[j] = ft_strtrim(args[j]);
		sh_controller(args, sh);
	}
	free(cmds);
	return (1);
}
