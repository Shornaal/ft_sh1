/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shcontroller.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/27 20:42:16 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/16 04:51:33 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>

void	sh_ctrl_unset(t_sh *sh, char **args)
{
	int		i;

	i = 0;
	if (!(args[1]))
	{
		ft_putendl_fd("unsetenv: Too few arguments.", 2);
		return ;
	}
	while (args[++i])
		env_unset(args[i], sh->env);
	sh->hydrate(sh);
}

void	sh_ctrl_set(t_sh *sh, char **args)
{
	int		i;

	i = 0;
	if (!(args[1]))
	{
		ft_putendl_fd("setenv: Too few arguments.", 2);
		return ;
	}
	while (args[++i])
		env_set(args[i], sh->env);
	sh->hydrate(sh);
}

void	sh_ctrl_env(t_sh *sh, char **args)
{
	env_ctrl(sh, args);
}

void	sh_ctrl_exe(t_sh *sh, char **args)
{
	if (!(ft_strncmp(args[0], "./", 2)))
		sh_exec_bin(sh, args[0], args, sh->pwd);
	else if ((args[0][0] == '/'))
		sh_exec_bin(sh, args[0], args, args[0]);
	else
	{
		if (!(sh->path) || !(sh->path[0]) || (sh->path[0] == '\0'))
		{
			ft_putendl_fd("sh: No $PATH variable in the environnement.", 2);
			return ;
		}
		sh_exec(sh, args[0], args);
	}
}

void	sh_controller(char **args, t_sh *sh)
{
	char	*cmd;

	cmd = *args;
	if (!ft_strcmp(cmd, "exit"))
	{
		ft_putendl("exit");
		exit(0);
	}
	if (!ft_strcmp(cmd, "setenv"))
		sh_ctrl_set(sh, args);
	else if (!ft_strcmp(cmd, "unsetenv"))
		sh_ctrl_unset(sh, args);
	else if (!ft_strcmp(cmd, "env"))
		sh_ctrl_env(sh, args);
	else if (!ft_strcmp(cmd, "cd"))
		sh_cd(sh, args);
	else
		sh_ctrl_exe(sh, args);
	free(args);
}
