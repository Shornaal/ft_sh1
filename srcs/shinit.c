/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shinit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 03:55:12 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/16 04:30:46 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh1.h>

void	env_init(t_sh *sh, t_env *env, char **envp)
{
	env->envp = envp;
	env->count = env_count;
	env_copy(env);
	sh->env = env;
	sh->hydrate(sh);
	if (!(sh->oldpwd))
		env_set(ft_strjoin("PWD=", sh_getcwd()), sh->env);
	if (!(sh->path))
		env_set("PATH=/bin:/usr/bin:/usr/sbin", sh->env);
	if (!(sh->lvl))
		env_set("SHLVL=1", sh->env);
	else
		env_set(ft_strjoin("SHLVL=", ft_itoa(sh->lvl + 1)), sh->env);
}

void	view_init(t_sh *sh, t_view *view)
{
	if (!(view = (t_view *)ft_memalloc(sizeof(t_view))))
		return ;
	view->ps1 = ft_strdup("(>o_O)> ~ ");
	sh->view = view;
	sh->view->display = env_display;
	sh->view->getcmd = sh_get_command;
}

t_sh	*sh_init(char **envp)
{
	t_sh		*sh;
	t_env		*env;
	t_view		*view;

	view = NULL;
	if (!(sh = (t_sh *)ft_memalloc(sizeof(t_sh))) ||
			(!(env = (t_env *)ft_memalloc(sizeof(t_env)))))
		return (NULL);
	sh->controller = sh_controller;
	sh->run = sh_run;
	sh->hydrate = sh_hydrate;
	sh->lvl = 0;
	env_init(sh, env, envp);
	view_init(sh, view);
	sh->hydrate(sh);
	return (sh);
}
