/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sh1.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/18 21:06:22 by tiboitel          #+#    #+#             */
/*   Updated: 2015/02/18 21:09:21 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SH1_H
# define FT_SH1_H

# include <libft.h>
# include <signal.h>
# include <sys/stat.h>

# define BUFF_SIZE 4096
# define CPINK "\033[0;36;40m"
# define CRED "\033[0;31;40m"
# define CCLR "\033[0m"

typedef struct	s_env
{
	char		**envp;
	int			(*count)(struct s_env *env);
}				t_env;

typedef struct	s_shview
{
	char		*ps1;
	void		(*display)(t_env *env);
	char		*(*getcmd)(struct s_shview *view);
}				t_view;

typedef struct	s_sh
{
	t_env		*env;
	t_view		*view;
	char		**path;
	char		*pwd;
	char		*home;
	char		*oldpwd;
	int			lvl;
	void		(*controller)(char **args, struct s_sh *sh);
	void		(*run)(struct s_sh *sh);
	void		(*hydrate)(struct s_sh *sh);
}				t_sh;

t_sh			*sh_init(char **envp);
void			sh_hydrate(t_sh *sh);
char			*sh_get_command(t_view *view);
char			*sh_getcwd(void);
int				file_isdir(const char *path);
int				sh_parse_command(char *cmd, t_sh *sh);
int				ft_isoption(const char *s);
int				ft_searchflags(int argc, char **argv,
		char *search, unsigned int *options);
void			sh_run(t_sh *sh);
int				sh_cd(t_sh *sh, char **args);
int				sh_exec(t_sh *sh, char *bin, char **args);
int				sh_exec_bin(t_sh *sh, char *bin, char **args, char *path);
void			sh_controller(char **args, t_sh *sh);
void			sh_ctrl_env(t_sh *sh, char **args);
void			sh_ctrl_unset(t_sh *sh, char **args);
void			sh_ctrl_set(t_sh *sh, char **args);
void			sh_ctrl_exe(t_sh *sh, char **args);
int				env_count(t_env *env);
int				env_isset(char *name, t_env *env);
int				env_ctrl(t_sh *sh, char **args);
void			env_cd(t_sh *sh);
void			env_display(t_env *env);
void			env_copy(t_env *env);
void			env_set(char *args, t_env *env);
void			env_unset(char *name, t_env *env);
void			signal_handler(int signum);
void			signal_catch(void);
void			wif_msg(int status);
#endif
